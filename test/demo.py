import mochila as m
import csv
import pprint


class Agent:
    def __init__(self, code, first_name, last_name, rank, active, peer, *args):
        """
        An agent that is used for declarative operations
        :param code: The agent id
        :param first_name: first name
        :param last_name: last name
        :param rank: rank in the system
        :param active: is the agent active
        :param peer: peer agents
        :param args: countries where the agent can be active
        """
        self.code = code
        self.first_name = first_name.strip()
        self.last_name = last_name.strip()
        self.rank = int(rank)
        self.active = (False, True)[active.strip() == 'TRUE']
        if peer:
            self.peers = peer.split('~')
        else:
            self.peers = []
        self.visited = list(args)

    def __str__(self):
        return self.code

    def __repr__(self):
        return self.code

people_csv = [
    "730-46-0957,Cass,      Lamba,       91,  TRUE,         ,Sweden,Switzerland,China,Georgia,",
    "186-01-5810,Barbara,   Rosewell,   487,  TRUE,         ,Indonesia,Poland,Albania,Portugal,",
    "424-16-0664,Elnore,    Dillestone,  95,  TRUE,730-46-0957,Ireland,Russia,Philippines,Japan,",
    "694-68-6118,Brig,      Derham,     367, FALSE,186-01-5810,Pakistan,Russia,China,Papua New Guinea,",
    "212-70-6483,Adelbert,  Michelet,   166, FALSE,186-01-5810~424-16-0664,Honduras,Spain,Brazil,Sierra Leone,",
    "824-34-4142,Aldon,     Craske,     437, FALSE,,Italy,Poland,Philippines,Argentina,",
    "539-35-1184,Valentine, Woolvin,     13, FALSE,694-68-6118,Spain,Bulgaria,Indonesia,,",
    "861-26-2185,Godard,    Gadie,       99, FALSE,212-70-6483,Portugal,Brazil,Thailand,China,",
    "368-95-4835,Etan,      Bumphries,  291, FALSE,         ,Tunisia,France,Mexico,Armenia,",
    "859-05-6244,Sutherlan, McElwee,    434,  TRUE,824-34-4142,China,Russia,Bosnia and Herzegovina,China,"]


agents = dict()
agentreader = csv.reader(people_csv, delimiter=',')
for row in agentreader:     # Load the agent data
    p = Agent(*row)
    agents[p.code] = p

agents_get = agents.get
for k, v in agents.items(): # Replace the string references for object references (peers)
    v.peers = [x for x in [agents_get(p) for p in v.peers] if x is not None]


def get_name(p):
    return "{} {}".format(p.first_name, p.last_name)


def get_code(p):
    return p.code

mochila = m.Sequence(agents.values())
d = mochila.aggregate(get_code, get_name)
pprint.pprint(d)

def get_peer(p):
    return p.peers


cl = mochila.closure(get_peer)
for peerh in cl:
    agent_peers = peerh.flatten()
    pprint.pprint(agent_peers)


def get_countries(agent):
    return [c for c in agent.visited if len(c) > 0]  # Ignore empty


a_countries = mochila.collect(get_countries).flatten().as_orderedset()
pprint.pprint(a_countries)


def visited_russia(agent):
    return "Russia" in agent.visited


def visited_colombia(agent):
    return "Colombia" in agent.visited


to_russia = mochila.exists(visited_russia)
pprint.pprint(to_russia)
to_colombia = mochila.exists(visited_colombia)
pprint.pprint(to_colombia)


def above_average(agent):
    return agent.rank < 500


def active_agent(agent):
    active = agent.active
    return active


average = mochila.for_all(above_average)
pprint.pprint(average)
active = mochila.for_all(active_agent)
pprint.pprint(active)


def visited_france(agent):
    return "France" in agent.visited


to_russia = mochila.one(visited_russia)
pprint.pprint(to_russia)
to_france = mochila.one(visited_france)
pprint.pprint(to_france)

to_russia = mochila.reject(visited_russia)
print("{} > {}".format(len(mochila), len(to_russia)))

check_russia = to_russia.select(visited_russia)
print(len(check_russia))

active_agents = mochila.select(active_agent).collect(get_name)
pprint.pprint(active_agents)

one_russia = mochila.select_one(visited_russia).code
pprint.pprint(one_russia)




