Uttiakajamatu (Multi Set)
-------------------------

.. autoclass:: mochila.MultiSet
    :members:

    Instances of MultiSet provide the following operations:

    .. data:: MultiSet + other

        Return a new MultiSet of items that are in MultiSet. If an item is also in *other*, then the result has the
        total count for that item.

    .. data:: MultiSet - other

        Return a new MultiSet of items that are in MultiSet. If an item is also in *other*, then the result has the
        number of items of the subtraction of the count of MultiSet - count of *other*. If the count is < 0, the item is
        not added to the result.

    .. data:: MultiSet <= other

        Test whether every element in the set is in *other* and if the count in MultiSet is <= than in *other*.

    .. data:: MultiSet < other

        Test whether the set is a proper subset of *other*, that is, set <= *other* and set != *other*.

    .. data:: MultiSet >= other

        Test whether every element in *other* is in the set and if the count in MultiSet is >= than in *other*.

    .. data:: MultiSet > other

        Test whether the set is a proper subset of *other*, that is, set <= *other* and set != *other*.

    .. data:: MultiSet & other

        Return a new MultiSet with elements that are in both sets, the count is the minimum of corresponding counts.

    .. data:: MultiSet | other

        Return a new MultiSet with elements that are in the MultiSet or *other*, the count is the maximum of value in
        either of the input counters.

    .. data:: MultiSet ^ other

        Return a new MultiSet with elements in either the MultiSet or *other* but not both.

    .. data:: MultiSet += other

        Update the MultiSet of items that are in *other*. If an item is also in *other*, then the result has the total
        count for that item.

    .. data:: MultiSet -= other

        Update the MultiSet of items that are in *other*. If an item is also in *other*, then the result has the number
        of items of the substraction of the count of MultiSet1 - count of *other*. If the count is < 0, the item is not
        added to the result.

    .. data:: MultiSet &= other

        Update the MultiSet with elements that are in both sets, the count is the minimum of corresponding counts.

    .. data:: MultiSet \|= other

        Update the MultiSet with elements that are in the MultiSet or *other*, the count is the maximum of value in
        either of the sets.

    .. data:: MultiSet ^= other

        Update the MultiSet with elements in either the MultiSet or *other* but not both.


    MultiSet implement all of the Set operations. MultiSet also provide the following additional methods: